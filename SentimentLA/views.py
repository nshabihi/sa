from django.shortcuts import render, redirect
from SentimentLA.forms import UserForm
from SentimentLA.models import SiteUser
from django.contrib import messages
from SentimentLA.ss import script 
from django.urls import reverse_lazy
from django.conf.urls import url
from SentimentLA.forms import DocumentForm
from SentimentLA.forms import UploadFileForm
from django.core.files.storage import FileSystemStorage
from SentimentLA.FileProcessing import checkFileType
import csv
from django.http import HttpResponse
import pandas as pd
import os
from django.conf import settings
from SentimentLA.file_to_chart_data import file_to_chart_data_main

def home(request):
#	return render(request, "indexS.html")
	return render(request, "index.html")	

def sentiment_analysis(request):
	#return render(request, "Sentiment_Analysis.html")
	return render(request, "analyze.html")

def header (request):
	return render(request, "header.html")

def footer (request):
	return render(request, "footer.html")

def contact (request):
	return render(request, "contact.html")  

def login(request):
	return render(request, "logIn.html")

def signup(request):
	return render(request, "signup.html")

def upload(request):
	return render(request, 'upload.html')

#new user
def user (request):
	if request.method == "POST":
		form = UserForm (request.POST)
		if form.is_valid():
			try:
				form.save()
				return redirect('/home')
			except:
				pass
	else:
		form = UserForm()
	return render (request, 'index.html' , {'form':form})

#view list of users
def view(request):
	site_user = SiteUser.objects.all()
	return render(request, "view.html", {'site_user': site_user})

#delete an existing user
def delete(request, id):
	site_user = SiteUser.objects.get(uid = id)
	site_user.delete()
	return redirect("/view")

#edit user
def edit(request, id):
	site_user = SiteUser.objects.get(uid=id)
	return render(request, "edit.html", {'site_user': site_user})

#update user
def update(request, id):
	idd = script(id)
	site_user = SiteUser.objects.get(uid=idd)
	form = UserForm(request.POST, instance = site_user)
	if form.is_valid:
		form.save()
		messages.success(request,"Record Updated!")
		return redirect("/view")


######### new
from django.core.files.storage import FileSystemStorage

def upload2 (request):
	context = {}
	if request.method == 'POST':
		uploaded_file = request.FILES['document']
		fs = FileSystemStorage ()
		name = fs.save (uploaded_file.name, uploaded_file)
		url = fs.url(name)
		context['url'] = fs.url(name)

		print(uploaded_file.name)
		print(uploaded_file.size)
		print(url)
		print(context)

	return render (request, 'Sentiment_Analysis.html', context)


def upload_file(request):
	context = {}
	if request.method == 'POST':
		uploaded_file = request.FILES['document']
		print(uploaded_file.name)
		print(uploaded_file.size)
		fs = FileSystemStorage()
		name = fs.save(uploaded_file.name, uploaded_file)
		
		message = "aaaaa"
		message = checkFileType (name)
		if message != 'NOK':
			context['url'] = fs.url(name)
		else:
			context['url'] = 'invalid file format'

#	return render(request, "Sentiment_Analysis.html", context)
	return render(request, "logIn.html", context)	
	
def results1(request):
	return render(request, 'Bubble_Chart.html')





def results1(request):
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="somefilename.csv"'

    writer = csv.writer(response)
    writer.writerow(['First row', 'Foo', 'Bar', 'Baz'])
    writer.writerow(['Second row', 'A', 'B', 'C', '"Testing"', "Here's a quote"])

    return response


def results(request):
    #handle uploaded file
    if request.method == 'POST':
    	uploaded_file = request.FILES['document']
    	#print(uploaded_file.name)
    	#print(uploaded_file.size)
    	fs = FileSystemStorage()
    	name = fs.save(uploaded_file.name, uploaded_file)
    	file = os.path.join(settings.MEDIA_ROOT, name)
    	#read from uploaded file
    	data = file_to_chart_data_main(file)
    	data_str_bubble = data[0]
    	data_str_line = data[1]
    	data_str_heatmap = data[2]
    	data_str_polar = "1,2,3,4,1,2&2,3,4,1,2,3&3,4,1,2,3,4&2,4,1,2,5,5&4,2,1,5,4,5"
    	data_str_scatter = "0,4/1/2017 7:34:11,2&0,5/1/2017 7:35:01,3&0,5/1/2017 7:35:12,1&0,4/1/2017 7:35:14,1&1,4/1/2017 7:36:34,3&1,5/1/2017 12:36:23,2&1,4/1/2017 7:36:12,5&1,4/1/2017 10:30:43,5&1,5/1/2017 7:36:32,2"
    	#data_str_heatmap = "2020-05-02,&,2020-05-28,&,anger,anticipation,disgust,fear,joy,negative,positive,sadness,surprise,trust,&,student-1,/,2020-05-05,1,1.43,&,student-3,/,2020-05-03,2,5.71,/,2020-05-25,2,1.43,/,2020-05-27,2,6.99,/,2020-05-28,2,5.71,/,2020-05-05,2,5.71,/,2020-05-04,2,6.99,/,2020-05-24,2,6.99,/,2020-05-26,2,5.71,&,student-2,/,2020-05-16,3,6.99,/,2020-05-17,3,5.86,/,2020-05-08,3,1.43,/,2020-05-09,3,0.0,/,2020-05-15,3,5.71,/,2020-05-11,3,5.71,/,2020-05-12,3,5.71,/,2020-05-18,3,6.43,/,2020-05-28,3,6.99,/,2020-05-26,3,6.99,/,2020-05-04,3,1.43,/,2020-05-03,3,5.71,/,2020-05-02,3,5.71,/,2020-05-10,3,3.14,/,2020-05-13,3,1.43,/,2020-05-14,3,6.99,/,2020-05-27,3,5.71,/,2020-05-07,3,5.71"

    else:
    	#read from existing file
    	#file = os.path.join(settings.MEDIA_ROOT, 'KF Sample (detailed).csv')
    	#read from text data
    	data_str_bubble = "group,anger,anticipation,disgust,fear,joy,negative,positive,sadness,surprise,trust,&,G1,0.78,1.50,0.39,0.89,1.39,2.44,3.22,1.06,1.44,2.61,&,G2,1.00,0.90,0.60,0.80,1.10,3.00,3.10,1.60,1.30,2.70"
    	data_str_polar = "1,2,3,4,1,2&2,3,4,1,2,3&3,4,1,2,3,4&2,4,1,2,5,5&4,2,1,5,4,5"
    	data_str_scatter = "0,4/1/2017 7:34:11,2&0,5/1/2017 7:35:01,3&0,5/1/2017 7:35:12,1&0,4/1/2017 7:35:14,1&1,4/1/2017 7:36:34,3&1,5/1/2017 12:36:23,2&1,4/1/2017 7:36:12,5&1,4/1/2017 10:30:43,5&1,5/1/2017 7:36:32,2"
    	data_str_line = "2020-03-05,&,2020-05-28,&,anger,anticipation,disgust,fear,joy,negative,positive,sadness,surprise,trust,&,Student-1,/,2020-03-05 00:54:26,0,3,0,0,2,0,3,0,1,3,&,Student-2,/,2020-05-28 23:46:19,2,0,0,0,1,5,3,3,1,4,/,2020-05-28 23:46:27,0,2,0,1,2,1,7,1,3,5,/,2020-05-28 23:46:49,2,0,2,1,0,5,0,2,0,0,/,2020-05-28 23:46:19,2,0,0,0,1,5,3,3,1,4,/,2020-05-28 23:46:27,0,2,0,1,2,1,7,1,3,5,/,2020-05-28 23:46:49,2,0,2,1,0,5,0,2,0,0,/,2020-05-28 23:46:49,0,3,0,1,3,1,3,0,2,3,/,2020-05-28 23:49:02,0,2,0,1,2,1,7,1,3,5,&,Student-3,/,2020-05-10 23:50:42,0,0,0,1,0,1,1,1,0,1,/,2020-05-10 23:54:52,2,0,2,1,0,5,0,2,0,0,/,2020-05-10 23:55:04,2,0,0,0,1,5,3,3,1,4,/,2020-05-10 23:56:27,0,3,0,1,3,1,3,0,2,3,/,2020-05-10 23:56:44,0,2,0,1,2,1,7,1,3,5,/,2020-05-10 23:56:58,2,0,2,1,0,5,0,2,0,0,/,2020-05-10 23:57:45,0,2,0,1,2,1,7,1,3,5,/,2020-03-28 01:07:03,2,3,1,3,3,6,5,0,1,5,/,2020-04-15 03:19:55,0,1,0,0,1,0,3,0,1,0,/,2020-05-10 23:43:16,1,3,0,2,2,5,5,2,2,3,/,2020-05-10 23:43:33,1,3,0,1,1,1,1,0,3,1,/,2020-05-10 23:49:51,0,0,0,0,0,0,0,0,0,0,/,2020-05-10 23:50:42,0,0,0,1,0,1,1,1,0,1,/,2020-05-10 23:54:52,2,0,2,1,0,5,0,2,0,0,/,2020-05-10 23:55:04,2,0,0,0,1,5,3,3,1,4,/,2020-05-10 23:56:27,0,3,0,1,3,1,3,0,2,3,/,2020-05-10 23:56:44,0,2,0,1,2,1,7,1,3,5,/,2020-05-10 23:56:58,2,0,2,1,0,5,0,2,0,0,/,2020-05-10 23:57:45,0,2,0,1,2,1,7,1,3,5"
    	data_str_heatmap = "2020-05-02,&,2020-05-28,&,anger,anticipation,disgust,fear,joy,negative,positive,sadness,surprise,trust,&,student-1,/,2020-05-05,1,1.43,&,student-3,/,2020-05-03,2,5.71,/,2020-05-25,2,1.43,/,2020-05-27,2,6.99,/,2020-05-28,2,5.71,/,2020-05-05,2,5.71,/,2020-05-04,2,6.99,/,2020-05-24,2,6.99,/,2020-05-26,2,5.71,&,student-2,/,2020-05-16,3,6.99,/,2020-05-17,3,5.86,/,2020-05-08,3,1.43,/,2020-05-09,3,0.0,/,2020-05-15,3,5.71,/,2020-05-11,3,5.71,/,2020-05-12,3,5.71,/,2020-05-18,3,6.43,/,2020-05-28,3,6.99,/,2020-05-26,3,6.99,/,2020-05-04,3,1.43,/,2020-05-03,3,5.71,/,2020-05-02,3,5.71,/,2020-05-10,3,3.14,/,2020-05-13,3,1.43,/,2020-05-14,3,6.99,/,2020-05-27,3,5.71,/,2020-05-07,3,5.71"

    print("data_str_polar:   "  + data_str_polar)
    return render(request, 'results.html', 
    	{"data_str_bubble" :data_str_bubble,
    	"data_str_line" :data_str_line,
    	"data_str_polar" : data_str_polar,
    	"data_str_scatter" : data_str_scatter,
    	"data_str_heatmap" : data_str_heatmap})
