var myVar = "2020-03-05,&,2020-05-28,&,anger,anticipation,disgust,fear,joy,negative,positive,sadness,surprise,trust,&,Student_1,/,2020-03-05 00:54:26,0,3,0,0,2,0,3,0,1,3,&,Student_2,/,2020-05-28 23:46:19,2,0,0,0,1,5,3,3,1,4,/,2020-05-28 23:46:27,0,2,0,1,2,1,7,1,3,5,/,2020-05-28 23:46:49,2,0,2,1,0,5,0,2,0,0,/,2020-05-28 23:46:19,2,0,0,0,1,5,3,3,1,4,/,2020-05-28 23:46:27,0,2,0,1,2,1,7,1,3,5,/,2020-05-28 23:46:49,2,0,2,1,0,5,0,2,0,0,/,2020-05-28 23:46:49,0,3,0,1,3,1,3,0,2,3,/,2020-05-28 23:49:02,0,2,0,1,2,1,7,1,3,5,&,Student_3,/,2020-05-10 23:50:42,0,0,0,1,0,1,1,1,0,1,/,2020-05-10 23:54:52,2,0,2,1,0,5,0,2,0,0,/,2020-05-10 23:55:04,2,0,0,0,1,5,3,3,1,4,/,2020-05-10 23:56:27,0,3,0,1,3,1,3,0,2,3,/,2020-05-10 23:56:44,0,2,0,1,2,1,7,1,3,5,/,2020-05-10 23:56:58,2,0,2,1,0,5,0,2,0,0,/,2020-05-10 23:57:45,0,2,0,1,2,1,7,1,3,5,/,2020-03-28 01:07:03,2,3,1,3,3,6,5,0,1,5,/,2020-04-15 03:19:55,0,1,0,0,1,0,3,0,1,0,/,2020-05-10 23:43:16,1,3,0,2,2,5,5,2,2,3,/,2020-05-10 23:43:33,1,3,0,1,1,1,1,0,3,1,/,2020-05-10 23:49:51,0,0,0,0,0,0,0,0,0,0,/,2020-05-10 23:50:42,0,0,0,1,0,1,1,1,0,1,/,2020-05-10 23:54:52,2,0,2,1,0,5,0,2,0,0,/,2020-05-10 23:55:04,2,0,0,0,1,5,3,3,1,4,/,2020-05-10 23:56:27,0,3,0,1,3,1,3,0,2,3,/,2020-05-10 23:56:44,0,2,0,1,2,1,7,1,3,5,/,2020-05-10 23:56:58,2,0,2,1,0,5,0,2,0,0,/,2020-05-10 23:57:45,0,2,0,1,2,1,7,1,3,5";
      


  var items = myVar.split(",&,");
  var startTime = items[0];
  startTime = startTime.split("-");
  var sd = new Date(parseInt(startTime[0]), parseInt(startTime[1])-1, parseInt(startTime[2]),0, 0, 0, 0);
  var startTime_epoch = Math.floor(sd.getTime());
  var endTime = items[1];
  endTime = endTime.split("-");
  var ed = new Date(parseInt(endTime[0]), parseInt(endTime[1])-1, parseInt(endTime[2]),0, 0, 0, 0);
  var endTime_epoch = Math.floor(ed.getTime());

  var emotion_categories = items[2].split(",");
  var emo_num = emotion_categories.length;


  var students_info = items.slice(3,items.length);

  std1 = students_info[2];
  std1 = std1.split(",/,");
  items = std1;
  time_intervals = Math.floor((endTime_epoch-startTime_epoch)/(24*3600000))+1;




  var dataSeries_emotions = (new Array(emo_num)).fill().map(function(){ return new Array(time_intervals).fill(0);});
  var item;
  for (j = 1; j < items.length ; j++)
    {
        item = String(items[j]);
        item = item.split(",");

       // time and date
        timeData = 0;
        date = item[0];
        date = date.split("-");

        var dt = new Date(parseInt(date[0]), parseInt(date[1])-1, parseInt(date[2]),0, 0, 0, 0);
        var epoch = Math.floor(dt.getTime());
        timeData = epoch;
        // END OF: time and date

        //assigning emotions to timeData
        timeData = (timeData - startTime_epoch)/(24*3600000); // the number of days in the chart
        timeData = Math.ceil(timeData);
        for (i=0 ; i<emo_num ; i++)
        {
          dataSeries_emotions[i][timeData]=parseInt(item[i+1]);
        }
    }

    for (i=0 ; i<emo_num ; i++)
    {
      document.write("dataSeries_emotions: ");
      document.write(dataSeries_emotions[i]);
    }

    var color_categories = [
                '#ffff00',//joy
                '#99cc00',//trust
                '#00cc00',//fear
                '#239090',//suprise
                '#0000e6',//sadness
                '#b300b3',//disgust
                '#cc0000',//anger
                '#ff8000',//anticipation
                '#cc12e0',//anger
                '#fc8000'//anticipation
              ];



    var seriesOptions = [];
    
    // forming chart input data
    for (i=0 ; i<emo_num ; i++)
    {
      seriesOptions[2*i] = {
        id : i,
        name : emotion_categories[i],
        data : dataSeries_emotions[i],
        stack : "student",
        color: color_categories[i]
      };

      seriesOptions[2*i+1] = {
        linkedTo : i,
        name : emotion_categories[i],
        data : dataSeries_emotions[i],
        stack : "average",
        color: color_categories[i]
      };
    }

    for (i=0 ; i<emo_num ; i++)
    {
      document.write("seriesOptions: ");
      document.write(seriesOptions[i]);
    }



  //var studens_num = students_info.length;
  //var total_days = (endTime_epoch - startTime_epoch)/(24*60*60*1000) +1;      
  //document.write("all: ");
  //document.write(students_info);
  //var std = [];
  //var arrayToPush = [];
  //var dataArray = [];
  //var seriesArray = [];
  //var current = startTime_epoch;


  Highcharts.chart('scatter_container', {

    chart: {
      type: 'column'
    },

    title: {
      text: 'Total fruit consumption, grouped by gender'
    },

    /*xAxis: {
      categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
    },*/

    xAxis: {
        type: 'datetime',
        dateTimeLabelFormats: {
          month: '%e. %b',
          year: '%b'
        },
        title: {
          text: 'Date'
        }
      },

    


    yAxis: {
      allowDecimals: false,
      min: 0,
      title: {
        text: 'Number of fruits'
      }
    },

    tooltip: {
      formatter: function () {
        return '<b>' + this.x + '</b><br/>' +
          this.series.name + ': ' + this.y + '<br/>' +
          'Total: ' + this.point.stackTotal;
      }
    },

    plotOptions: {
      column: {
        stacking: 'normal'
      }
    },

    series: seriesOptions/*[{
      name: 'John',
      data: [5, 3, 4, 7, 2],
      stack: 'male',
      color: '#efefef'
    }, {
      name: 'Joe',
      data: [3, 4, 4, 2, 5],
      stack: 'female',
      color: '#efefef'
    }, {
      name: 'Jane',
      data: [2, 5, 6, 2, 1],
      stack: 'female'
    }, {
      name: 'Janet',
      data: [3, 0, 4, 4, 3],
      stack: 'female'
    },
    {
      name: 'Julie',
      data: [5, 3, 4, 7, 2],
      stack: 'male'
    }]*/
  });