app.component('chartbox',{
    data: function(){
        return {
            charts: [
                {order:1, div_id: 'bubble_container', header_id:'bubble_container_header', title:'title1', height: '450px', width: '800px'},
                {order:2, div_id: 'line_container', header_id:'line_container_header', title:'title2', height: '600px', width: '1000px'},
                {order:3, div_id: 'polar_container', header_id:'polar_container_header', title:'title3', height: '600px', width: '1000px'},
                {order:4, div_id: 'scatter_container', header_id:'scatter_container_header', title:'title4', height: '600px', width: '1000px'},
                {order:5, div_id: 'heatmap_container', header_id:'heatmap_container_header', title:'title5', height: '600px', width: '1000px'}
            ]  
        }
    },
    template:
    /*html*/
    `<div id="charts" class="container charts" >
        <div v-for="(x, index) in charts" class="row chart card"> 
            <h2 :id="x.header_id" class="card-header">{{x.title}}
            
                <i class="fas fa-angle-up"></i>
            </h2>
            <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-primary form-check-label active">
                    <input class="form-check-input" type="radio" name="options" :id="x.title" autocomplete="off" checked>
                    Emotion
                </label>
                <label class="btn btn-primary form-check-label">
                    <input class="form-check-input" type="radio" name="options" :id="x.title" autocomplete="off"> Sentiment
                </label>
            </div>
            <div class="card-body" :id="x.div_id" :style="{width:x.width, height:x.height}"></div>
        </div>
    </div>`,
    props: ['chatrs']
})