var seriesArray = [];
var dataArray = [];
var date;
var timeData;
var dataSeries_emotions = [];
var time2;
var seriesOptions = [];
var time;

var myVar = document.getElementById("myVar_line").value;

intialization(function() {
    createChart_line()
});

function intialization(callback){

//document.write("myvar: ");
//document.write(myVar);

var items = myVar.split(",&,");
var startTime = items[0];
startTime = startTime.split("-");
var sd = new Date(parseInt(startTime[0]), parseInt(startTime[1])-1, parseInt(startTime[2]),0, 0, 0, 0);
var startTime_epoch = Math.floor(sd.getTime());
var endTime = items[1];
endTime = endTime.split("-");
var ed = new Date(parseInt(endTime[0]), parseInt(endTime[1])-1, parseInt(endTime[2]),0, 0, 0, 0);
var endTime_epoch = Math.floor(ed.getTime());
var currentTime = startTime;

emotion_categories = items[2].split(",");
emo_num = emotion_categories.length;
//document.write("emotion_categories");
//document.write(emotion_categories);
//document.write("emo_num");
//document.write(emo_num);
students_info = items.slice(3,items.length);
//document.write("all: ");
//document.write(students_info);
std1 = students_info[1];
std1 = std1.split(",/,");
items = std1;


//find list of student
var student_list = [];
for (var i = 0; i<students_info.length ; i++)
{
  std_temp = students_info[i];
  std_temp = std_temp.split(",/,");
  student_list.push(std_temp[0]);
}


// time data
time_intervals = Math.floor((endTime_epoch-startTime_epoch)/3600000)+1;
var dataSeries_emotions = (new Array(emo_num)).fill().map(function(){ return new Array(time_intervals).fill(0);});
var item;
for (j = 1; j < items.length ; j++)
  {
      item = String(items[j]);
      item = item.split(",");

     // time and date
      timeData = 0;
      date = item[0];
      date = date.split(" "); 
      if (date.length == 1) { break; }
      
      time = date[1];
      date = date[0];
      date = date.split("-");
      time = time.split(":");

      var dt = new Date(parseInt(date[0]), parseInt(date[1])-1, parseInt(date[2]),parseInt(time[0]), parseInt(time[1]), parseInt(time[2]), 0);
      var epoch = Math.floor(dt.getTime());
      timeData = epoch;
      // END OF: time and date

      //assigning emotions to timeData
      timeData = (timeData - startTime_epoch)/3600000; // the number of hours in the chart
      timeData = Math.ceil(timeData);
      for (i=0 ; i<emo_num ; i++)
      {
        dataSeries_emotions[i][timeData]=parseInt(item[i+1]);
      }
  }

  for (i=0 ; i<emo_num ; i++)
  {
    //document.write("dataSeries_emotions: ");
    //document.write(dataSeries_emotions[i]);
  }


  
  // forming chart input data
  for (i=0 ; i<emo_num ; i++)
  {
    seriesOptions[i] = {
      name : emotion_categories[i],
      data : dataSeries_emotions[i],
      pointStart : Date.UTC(parseInt(startTime[0]), parseInt(startTime[1])-1, parseInt(startTime[2])),
      pointInterval : 3600*1000
    };
  }

  callback();
}

function createChart_line() {
  Highcharts.stockChart('line_container', {
    chart: {
      renderTo: 'line_container',
      zoomType: 'xy',
      panning: true,
      panKey: 'shift',
      plotBorderWidth: 1
    },

    title: {
      text: 'Emotion Change'
    },

    subtitle: {
      text: "This figure shows a particular student's emotion change. Select a student from the list."
    },


    yAxis: {
      title: {
        text: 'Y axis'
      },
      max:10,
      min:0

    },

    xAxis: {
      type: 'datetime',
      dateTimeLabelFormats: {
        month: '%e. %b',
        year: '%b'
      },
      title: {
        text: 'Date'
      }
    },
    plotOptions: {
      series: {
        label: {
          connectorAllowed: false
        },

      }
    },


    credits: {
      enabled: false
    },

    legend: {
      enabled: true,
      align: 'center',
      // backgroundColor: '#FCFFC5',
      // borderColor: 'black',
      // borderWidth: 2,
      layout: 'horizontal',
      verticalAlign: 'bottom',
      y: 0,
      //shadow: true
    },


    series: seriesOptions,

    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  });

}


