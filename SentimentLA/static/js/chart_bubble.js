var teamName;
var bubblesize = 27;
var arraytoPush = [];
var dataArray = [];
var seriesArray = [];
var teamCounter = 1;
var myVar = document.getElementById("myVar_bubble").value;
var items = myVar.split(",&,");
var emotion_categories = items[0].split(",");
emotion_categories.shift();
emotions_num = emotion_categories.length;

intialization(function() {
    createChart_bubble()
});

function intialization(callback){
  

  var groups = items.length;
  for (j = 1; j < groups ; j++)
  {
    data = items[j].split(",");
    teamName = data[0];
    var i;
    for (i = 0; i <emotions_num ; i++){
      arrayToPush = [i,parseInt(data[i+1]),bubblesize,j,teamName];
      dataArray.push(arrayToPush);
      //document.write("test4");
      //document.write(arrayToPush);  
    }
    seriesArray.push({type:"bubble",color:randomColor(),data:dataArray});
    dataArray = [];
  }

  callback();
}   

function randomColor(){
  var symbols, color;
  symbols = "0123456789ABCDEF";

  color = "#";
  for (var i = 0;i<6;i++){
    color = color + symbols[Math.floor(Math.random() * 16)];
  }
  console.log(color);
  return color;
}

function createChart_bubble() {
  Highcharts.chart('bubble_container', {


    chart: {
      type: 'bubble',
      plotBorderWidth: 1,
      zoomType: 'xy'
    },

    legend: {
      enabled: false
    },

    title: {
      text: 'Emotion Comparison for Groups'
    },

    subtitle: {
      text: ''
    },

    accessibility: {
      point: {
        valueDescriptionFormat: '{index}. {point.name}, Catagory: {point.x}, Score: {point.y},'
      }
    },

    xAxis: {
      gridLineWidth: 1,
      title: {
        text: ''
      },
      categories: emotion_categories,

      tickInterval: 1,

      accessibility: {
        rangeDescription: ''
      }
    },

    yAxis: {
      startOnTick: false,
      endOnTick: false,
      title: {
        text: ''
      },
      labels: {
        format: '{value} '
      },
      maxPadding: 0.2,

      accessibility: {
        rangeDescription: ''
      }
    },
    legend: {
      // layout: 'vertical',
      // align:'left',
      // verticalAlign:'top',
      // x: 100,
      // y: 70,
      // floating: true,
      backgroundColor: Highcharts.defaultOptions.chart.backgroundColor,
      borderWidth: 1,
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle',
      borderWidth: 0
    },

    tooltip: {
      useHTML: true,
      headerFormat: '<table>',
      pointFormat: '<tr><th colspan="2"><h3>{point.name}</h3></th></tr>' +

        '<tr><th>Score:</th><td>{point.y}</td></tr>',
      footerFormat: '</table>',
      followPointer: true
    },

    plotOptions: {
      series: {
        keys:['x','y','z','studentNum','name'],

        dataLabels: {
          enabled: true,
          format: '{point.studentNum}'
        }
      }
    },

    credits: {
      enabled: false
    },
    series:seriesArray
  });
}