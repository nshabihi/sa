var seriesArray = [];
var items = [];
var item_str = [];
var item_num = [];
var names = ['Frustrated', 'Confused','Bored','Happy','Motivated'];
var temp = [];
var myVar = document.getElementById("myVar_polar").value;


intialization_polar(function() {
    createChart_polar()
}); 

function intialization_polar(callback){

    items = myVar.split("&");

    //NOT working
    for (var i=0; i< items.length ; i++){
      item_str = items[i].split(",");
      item_num = item_str.map(Number);
      seriesArray.push({name: names[i],data:item_num});

      temp = item_num;
      item_num = [];
    }
    //NOT working

    seriesArray = []
    for (var i=0; i< items.length ; i++){
      item_str = items[i].split(",");
      item_num = item_str.map(Number);
      seriesArray.push(item_num)
    }


    callback();
  }   

function createChart_polar() {
  Highcharts.chart('polar_container', {

    chart: {
      polar: true,
      type: 'column'
    },

    xAxis: {
      type: 'category',
      categories: [
              "Brainstorming","Interface design",'Implementation','Documentation','Experimentation','Install Instructions'
          ],
    },

    credits: {
      enabled: false
    },
    
    
    series: [{
        name:names[0],
        data: seriesArray[0]
    }, {
        name: names[1],
        data: seriesArray[1]
    }, {
        name: names[2],
        data: seriesArray[2]
    },{
        name:names[3],
        data:seriesArray[3]
    },{
        name:names[4],
        data:seriesArray[4]
    }
    ]

      //series: seriesArray

  });
}


